<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'Auth\API\LoginController@login');

Route::post('provinces', 'IndonesiaController@provinces')->name('provinces');
Route::post('regencies', 'IndonesiaController@regencies')->name('regencies');
Route::post('districts', 'IndonesiaController@districts')->name('districts');
Route::post('villages', 'IndonesiaController@villages')->name('villages');
Route::post('province', 'IndonesiaController@province')->name('province');
Route::post('regency', 'IndonesiaController@regency')->name('regency');
Route::post('district', 'IndonesiaController@district')->name('district');
Route::post('village', 'IndonesiaController@village')->name('village');

Route::post('kodams', 'MilitaryRegionController@kodams')->name('kodams');
Route::post('korems', 'MilitaryRegionController@korems')->name('korems');
Route::post('kodims', 'MilitaryRegionController@kodims')->name('kodims');
Route::post('koramils', 'MilitaryRegionController@koramils')->name('koramils');
Route::post('kodam', 'MilitaryRegionController@kodam')->name('kodam');
Route::post('korem', 'MilitaryRegionController@korem')->name('korem');
Route::post('kodim', 'MilitaryRegionController@kodim')->name('kodim');
Route::post('koramil', 'MilitaryRegionController@koramil')->name('koramil');
