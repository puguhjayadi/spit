<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('', function() {
    return redirect('login');
});

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('register/token', 'Auth\API\RegisterController@token');
Route::post('register/inteldam', 'Auth\API\RegisterController@storeInteldam');

Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register')->name('doRegister');

Route::get('/home', 'HomeController@index')->name('home');

Route::post('provinces', 'IndonesiaController@provinces')->name('provinces');
Route::post('regencies', 'IndonesiaController@regencies')->name('regencies');
Route::post('districts', 'IndonesiaController@districts')->name('districts');
Route::post('villages', 'IndonesiaController@villages')->name('villages');
Route::post('province', 'IndonesiaController@province')->name('province');
Route::post('regency', 'IndonesiaController@regency')->name('regency');
Route::post('district', 'IndonesiaController@district')->name('district');
Route::post('village', 'IndonesiaController@village')->name('village');


