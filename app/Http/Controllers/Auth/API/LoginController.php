<?php

namespace App\Http\Controllers\Auth\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\User;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        try {
            $this->validate($request, [
                'email' => 'required|string',
                'password' => 'required|string'
            ]);
            $user = User::where('email', $request->email)->first();
            if ($user) {
                if (Hash::check($request->password, $user->password)) {
                    return response()->json(['data' => $user]);
                } else {
                    throw new \Exception('Password salah', 422);
                    
                }
            } else {
                throw new \Exception('Email tidak ditemukan', 404);
                
            }
        } catch (\Throwable $th) {
            if (method_exists($th, 'errors')) {
                $error = $th->errors();
            } else {
                $error = $th->getMessage();
            }
            return response()->json(['error' => $error], $th->getCode() == 0? 500 : $th->getCode());
        }
    }
}
