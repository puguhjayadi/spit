<?php

namespace App\Http\Controllers\Auth\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\User;

class RegisterController extends Controller
{

	public function token()
	{
		return csrf_token();
	}

	public function storeInteldam(Request $request)
	{

		/*

		{
			"name" : "Andi",
			"email" : "andi@gmail.com",
			"password" : 123,
			"phone_number" : "081234",
			"phone_imei" : "12345"
		}

		*/
		$post = $request->all();

		DB::beginTransaction();

		try {

			$validator = \Validator::make(
				[
					'email' => $post['email'],
					'phone_imei' => $post['phone_imei']
				],
				[
					'email' => 'required|unique:users,email',
					'phone_imei' => 'required|unique:users,phone_imei',
				]
			);

			if ($validator->fails()) {
				return response()->json(['message' => $validator->messages()], 422);
			}


			$user = $post;
			unset($user['password']);
			$user['password'] = bcrypt($post['password']);
			$userId = User::create($user)->id;
			
			$response = [
				'status' => 200,
				'message' => "Success save user",
				'user' => $user,
			];
			DB::commit();


			return response()->json($response, 200);


		} catch(\Exception $e){

			DB::rollback();
			return response()->json(['message' => $e->getMessage() ]);
		}

	}
}
