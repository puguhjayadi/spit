<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MilitaryRegionController extends Controller
{
    public function kodams(Request $request)
    {
        if ($request->q) {
            $data = DB::table('kodams')->where('name', 'like', '%'.$request->q.'%')->get();
        } else {
            $data = DB::table('kodams')->get();
        }
        return response()->json(['data' => $data]);
    }

    public function kodam(Request $request)
    {
        try {
            $data = DB::table('kodams')->where('id', $request->kodam_id)->first();
            if ($data) {
                if ($request->text == true) {
                  $data = $data->name;
                }
                return response()->json(['data' => $data]);
            } else {
                throw new \Exception('Data tidak ditemukan', 404);
            }
        } catch (\Throwable $th) {
            if (method_exists($th, 'errors')) {
                $error = $th->errors();
            } else {
                $error = $th->getMessage();
            }
            return response()->json(['error' => $error], $th->getCode() == 0? 500 : $th->getCode());
        }
    }

    public function korems(Request $request)
    {
        if ($request->q) {
            $data = DB::table('korems')->where('kodam_id', $request->kodam_id)->where('name', 'like', '%'.$request->q.'%')->get();
        } else {
            $data = DB::table('korems')->where('kodam_id', $request->kodam_id)->get();
        }
        return response()->json(['data' => $data]);
    }

    public function korem(Request $request)
    {
        try {
            $data = DB::table('korems')->where('id', $request->korem_id)->first();
            if ($data) {
                if ($request->text == true) {
                  $data = $data->name;
                }
                return response()->json(['data' => $data]);
            } else {
                throw new \Exception('Data tidak ditemukan', 404);
            }
        } catch (\Throwable $th) {
            if (method_exists($th, 'errors')) {
                $error = $th->errors();
            } else {
                $error = $th->getMessage();
            }
            return response()->json(['error' => $error], $th->getCode() == 0? 500 : $th->getCode());
        }
    }

    public function kodims(Request $request)
    {
        if ($request->q) {
            $data = DB::table('kodims')->where('korem_id', $request->korem_id)->where('name', 'like', '%'.$request->q.'%')->get();
        } else {
            $data = DB::table('kodims')->where('korem_id', $request->korem_id)->get();
        }
        return response()->json(['data' => $data]);
    }

    public function kodim(Request $request)
    {
        try {
            $data = DB::table('kodims')->where('id', $request->kodim_id)->first();
            if ($data) {
                if ($request->text == true) {
                  $data = $data->name;
                }
                return response()->json(['data' => $data]);
            } else {
                throw new \Exception('Data tidak ditemukan', 404);
            }
        } catch (\Throwable $th) {
            if (method_exists($th, 'errors')) {
                $error = $th->errors();
            } else {
                $error = $th->getMessage();
            }
            return response()->json(['error' => $error], $th->getCode() == 0? 500 : $th->getCode());
        }
    }

    public function koramils(Request $request)
    {
        if ($request->q) {
            $data = DB::table('koramils')->where('kodim_id', $request->kodim_id)->where('name', 'like', '%'.$request->q.'%')->get();
        } else {
            $data = DB::table('koramils')->where('kodim_id', $request->kodim_id)->get();
        }
        return response()->json(['data' => $data]);
    }

    public function koramil(Request $request)
    {
        try {
            $data = DB::table('koramils')->where('id', $request->koramil_id)->first();
            if ($data) {
                if ($request->text == true) {
                  $data = $data->name;
                }
                return response()->json(['data' => $data]);
            } else {
                throw new \Exception('Data tidak ditemukan', 404);
            }
        } catch (\Throwable $th) {
            if (method_exists($th, 'errors')) {
                $error = $th->errors();
            } else {
                $error = $th->getMessage();
            }
            return response()->json(['error' => $error], $th->getCode() == 0? 500 : $th->getCode());
        }
    }
}
