<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Koramil extends Model
{
	public function kodam()
	{
		return $this->belongsTo('App\Kodam');
	}

	public function korem()
	{
		return $this->belongsTo('App\Korem');
	}

	public function kodim()
	{
		return $this->belongsTo('App\Kodim');
	}
}
