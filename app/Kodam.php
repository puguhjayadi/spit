<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kodam extends Model
{
	public function korems()
	{
		return $this->hasMany('App\Korem');
	}

	public function kodims()
	{
		return $this->hasMany('App\Kodim');
	}

	public function koramils()
	{
		return $this->hasMany('App\Koramil');
	}
}
