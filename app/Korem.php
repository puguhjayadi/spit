<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Korem extends Model
{
	public function kodam()
	{
		return $this->belongsTo('App\Kodam');
	}

	public function kodims()
	{
		return $this->hasMany('App\Kodim');
	}
}
