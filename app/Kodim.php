<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kodim extends Model
{
    public function kodam()
	{
		return $this->belongsTo('App\Kodam');
	}

	public function korem()
	{
		return $this->belongsTo('App\Korem');
	}

	public function koramils()
	{
		return $this->hasMany('App\Koramil');
	}
}
