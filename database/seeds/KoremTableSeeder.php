<?php

use Illuminate\Database\Seeder;

class KoremTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table("korems")->insert([
    		array(
    			"id" => 1, 
    			"name" => "Komando Resor Militer 011/Lilawangsa", 
    			"kodam_id" => 1), 
    		array(
    			"id" => 2, 
    			"name" => "Komando Resor Militer 012/Teuku Umar", 
    			"kodam_id" => 1), 
    		array(
    			"id" => 3, 
    			"name" => "Komando Resor Militer 022/Pantai Timur", 
    			"kodam_id" => 2), 
    		array(
    			"id" => 4, 
    			"name" => "Komando Resor Militer 023/Kawal Samudera", 
    			"kodam_id" => 2), 
    		array(
    			"id" => 5, 
    			"name" => "Komando Resor Militer 031/Wirabima", 
    			"kodam_id" => 2), 
    		array(
    			"id" => 6, 
    			"name" => "Komando Resor Militer 032/Wirabraja", 
    			"kodam_id" => 2), 
    		array(
    			"id" => 7, 
    			"name" => "Komando Resor Militer 033/Wirapratama", 
    			"kodam_id" => 2), 
    		array(
    			"id" => 8, 
    			"name" => "Komando Resor Militer 041/Garuda Emas", 
    			"kodam_id" => 3), 
    		array(
    			"id" => 9, 
    			"name" => "Komando Resor Militer 042/Garuda Putih", 
    			"kodam_id" => 3), 
    		array(
    			"id" => 10, 
    			"name" => "Komando Resor Militer 043/Garuda Hitam", 
    			"kodam_id" => 3), 
    		array(
    			"id" => 11, 
    			"name" => "Komando Resor Militer 044/Garuda Dempo", 
    			"kodam_id" => 3), 
    		array(
    			"id" => 12, 
    			"name" => "Komando Resor Militer 045/Garuda Jaya", 
    			"kodam_id" => 3), 
    		array(
    			"id" => 13, 
    			"name" => "Komando Resor Militer 051/Wijayakarta", 
    			"kodam_id" => 4), 
    		array(
    			"id" => 14, 
    			"name" => "Komando Resor Militer 052/Wijayakrama", 
    			"kodam_id" => 4), 
    		array(
    			"id" => 15, 
    			"name" => "Komando Resor Militer 061/Suryakencana", 
    			"kodam_id" => 5), 
    		array(
    			"id" => 16, 
    			"name" => "Komando Resor Militer 062/Tarumanegara", 
    			"kodam_id" => 5), 
    		array(
    			"id" => 17, 
    			"name" => "Komando Resor Militer 063/Sunan Gunung Jati", 
    			"kodam_id" => 5), 
    		array(
    			"id" => 18, 
    			"name" => "Komando Resor Militer 064/Maulana Yusuf", 
    			"kodam_id" => 5), 
    		array(
    			"id" => 19, 
    			"name" => "Komando Resor Militer 071/Wijayakusuma", 
    			"kodam_id" => 6), 
    		array(
    			"id" => 20, 
    			"name" => "Komando Resor Militer 072/Pamungkas", 
    			"kodam_id" => 6), 
    		array(
    			"id" => 21, 
    			"name" => "Komando Resor Militer 073/Makutarama", 
    			"kodam_id" => 6), 
    		array(
    			"id" => 22, 
    			"name" => "Komando Resor Militer 074/Warastratama", 
    			"kodam_id" => 6), 
    		array(
    			"id" => 23, 
    			"name" => "Komando Resor Militer 081/Dhirotsaha Jaya", 
    			"kodam_id" => 7), 
    		array(
    			"id" => 24, 
    			"name" => "Komando Resor Militer 082/Panca Yudha Jaya", 
    			"kodam_id" => 7), 
    		array(
    			"id" => 25, 
    			"name" => "Komando Resor Militer 083/Bhaladika Jaya", 
    			"kodam_id" => 7), 
    		array(
    			"id" => 26, 
    			"name" => "Komando Resor Militer 084/Bhaskara Jaya", 
    			"kodam_id" => 7), 
    		array(
    			"id" => 27, 
    			"name" => "Komando Resor Militer 091/Aji Surya Natakesuma", 
    			"kodam_id" => 8), 
    		array(
    			"id" => 28, 
    			"name" => "Komando Resor Militer 092/Maharajalilla", 
    			"kodam_id" => 8), 
    		array(
    			"id" => 29, 
    			"name" => "Komando Resor Militer 101/Antasari", 
    			"kodam_id" => 8), 
    		array(
    			"id" => 30, 
    			"name" => "Komando Resor Militer 102/Panju Panjung", 
    			"kodam_id" => 10), 
    		array(
    			"id" => 31, 
    			"name" => "Komando Resor Militer 121/Alambhana Wanawai", 
    			"kodam_id" => 10), 
    		array(
    			"id" => 32, 
    			"name" => "Komando Resor Militer 131/Santiago", 
    			"kodam_id" => 11), 
    		array(
    			"id" => 33, 
    			"name" => "Komando Resor Militer 132/Tadulako", 
    			"kodam_id" => 11), 
    		array(
    			"id" => 34, 
    			"name" => "Komando Resor Militer 133/Nani Wartabone", 
    			"kodam_id" => 11), 
    		array(
    			"id" => 35, 
    			"name" => "Komando Resor Militer 141/Toddopuli", 
    			"kodam_id" => 12), 
    		array(
    			"id" => 36, 
    			"name" => "Komando Resor Militer 142/Taroada Tarogau", 
    			"kodam_id" => 12), 
    		array(
    			"id" => 37, 
    			"name" => "Komando Resor Militer 143/Halu Oleo", 
    			"kodam_id" => 12), 
    		array(
    			"id" => 38, 
    			"name" => "Komando Resor Militer 151/Binaiya", 
    			"kodam_id" => 13), 
    		array(
    			"id" => 39, 
    			"name" => "Komando Resor Militer 152/Baabullah", 
    			"kodam_id" => 13), 
    		array(
    			"id" => 40, 
    			"name" => "Komando Resor Militer 161/Wirasakti", 
    			"kodam_id" => 9), 
    		array(
    			"id" => 41, 
    			"name" => "Komando Resor Militer 162/Wirabhakti", 
    			"kodam_id" => 9), 
    		array(
    			"id" => 42, 
    			"name" => "Komando Resor Militer 163/Wirasatya", 
    			"kodam_id" => 9), 
    		array(
    			"id" => 43, 
    			"name" => "Komando Resor Militer 172/Praja Wira Yakthi", 
    			"kodam_id" => 14), 
    		array(
    			"id" => 44, 
    			"name" => "Komando Resor Militer 173/Praja Vira Braja", 
    			"kodam_id" => 14), 
    		array(
    			"id" => 45, 
    			"name" => "Komando Resor Militer 174/Anim Ti Waninggap", 
    			"kodam_id" => 14), 
    		array(
    			"id" => 46, 
    			"name" => "Komando Resor Militer 181/Praja Vira Tama", 
    			"kodam_id" => 15)
    	]);
}
}
