<?php

use Illuminate\Database\Seeder;

class KodimTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table("kodims")->insert([
    		array(
    			"id" => 1, 
    			"name" => "KODIM 0606/KOTA BOGOR", 
    			"korem_id" => 15, 
    			"kodam_id" => 1), 
    		array(
    			"id" => 2, 
    			"name" => "KODIM 0607/KOTA SUKABUMI", 
    			"korem_id" => 15, 
    			"kodam_id" => 1), 
    		array(
    			"id" => 3, 
    			"name" => "KODIM 0608/CIANJUR", 
    			"korem_id" => 15, 
    			"kodam_id" => 1), 
    		array(
    			"id" => 4, 
    			"name" => "KODIM 0621/KAB. BOGOR", 
    			"korem_id" => 15, 
    			"kodam_id" => 1), 
    		array(
    			"id" => 5, 
    			"name" => "KODIM 0622/PELABUHAN RATU", 
    			"korem_id" => 15, 
    			"kodam_id" => 1), 
    		array(
    			"id" => 6, 
    			"name" => "KODIM 0609/CIMAHI", 
    			"korem_id" => 16, 
    			"kodam_id" => 1), 
    		array(
    			"id" => 7, 
    			"name" => "KODIM 0610/SUMEDANG", 
    			"korem_id" => 16, 
    			"kodam_id" => 1), 
    		array(
    			"id" => 8, 
    			"name" => "KODIM 0611/GARUT", 
    			"korem_id" => 16, 
    			"kodam_id" => 1), 
    		array(
    			"id" => 9, 
    			"name" => "KODIM 0612/TASIKMALAYA", 
    			"korem_id" => 16, 
    			"kodam_id" => 1), 
    		array(
    			"id" => 10, 
    			"name" => "KODIM 0613/CIAMIS", 
    			"korem_id" => 16, 
    			"kodam_id" => 1), 
    		array(
    			"id" => 11, 
    			"name" => "KODIM 0624/SOREANG", 
    			"korem_id" => 16, 
    			"kodam_id" => 1), 
    		array(
    			"id" => 12, 
    			"name" => "KODIM 0604/KARAWANG", 
    			"korem_id" => 17, 
    			"kodam_id" => 1), 
    		array(
    			"id" => 13, 
    			"name" => "KODIM 0605/SUBANG", 
    			"korem_id" => 17, 
    			"kodam_id" => 1), 
    		array(
    			"id" => 14, 
    			"name" => "KODIM 0614/KOTA CIREBON", 
    			"korem_id" => 17, 
    			"kodam_id" => 1), 
    		array(
    			"id" => 15, 
    			"name" => "KODIM 0615/KUNINGAN", 
    			"korem_id" => 17, 
    			"kodam_id" => 1), 
    		array(
    			"id" => 16, 
    			"name" => "KODIM 0616/INDRAMAYU", 
    			"korem_id" => 17, 
    			"kodam_id" => 1), 
    		array(
    			"id" => 17, 
    			"name" => "KODIM 0617/MAJALENGKA", 
    			"korem_id" => 17, 
    			"kodam_id" => 1), 
    		array(
    			"id" => 18, 
    			"name" => "KODIM 0619/PURWAKARTA", 
    			"korem_id" => 17, 
    			"kodam_id" => 1), 
    		array(
    			"id" => 19, 
    			"name" => "KODIM 0620/KAB CIREBON", 
    			"korem_id" => 17, 
    			"kodam_id" => 1), 
    		array(
    			"id" => 20, 
    			"name" => "KODIM 0601/PANDEGLANG", 
    			"korem_id" => 18, 
    			"kodam_id" => 1), 
    		array(
    			"id" => 21, 
    			"name" => "KODIM 0602/SERANG", 
    			"korem_id" => 18, 
    			"kodam_id" => 1), 
    		array(
    			"id" => 22, 
    			"name" => "KODIM 0603/LEBAK", 
    			"korem_id" => 18, 
    			"kodam_id" => 1), 
    		array(
    			"id" => 23, 
    			"name" => "KODIM 0623/CILEGON", 
    			"korem_id" => 18, 
    			"kodam_id" => 1), 
    		array(
    			"id" => 24, 
    			"name" => "KODIM 0618/BS", 
    			"korem_id" => 18, 
    			"kodam_id" => 1)
    	]);
    }
}
