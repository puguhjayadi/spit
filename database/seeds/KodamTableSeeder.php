<?php

use Illuminate\Database\Seeder;

class KodamTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table("kodams")->insert([
    		array("id" => 1, "name" => "Kodam Iskandar Muda"), 
    		array("id" => 2, "name" => "Kodam I/Bukit Barisan"), 
    		array("id" => 3, "name" => "Kodam II/Sriwijaya"), 
    		array("id" => 4, "name" => "Kodam Jaya"), 
    		array("id" => 5, "name" => "Kodam III/Siliwangi"), 
    		array("id" => 6, "name" => "Kodam IV/Diponegoro"), 
    		array("id" => 7, "name" => "Kodam V/Brawijaya"), 
    		array("id" => 8, "name" => "Kodam VI/Mulawarman"), 
    		array("id" => 9, "name" => "Kodam IX/Udayana"), 
    		array("id" => 10, "name" => "Kodam XII/Tanjungpura"), 
    		array("id" => 11, "name" => "Kodam XIII/Merdeka"), 
    		array("id" => 12, "name" => "Kodam XIV/Hasanuddin"), 
    		array("id" => 13, "name" => "Kodam XVI/Pattimura"), 
    		array("id" => 14, "name" => "Kodam XVII/Cenderawasih"), 
    		array("id" => 15, "name" => "Kodam XVIII/Kasuari")
    	]);
    }
}
