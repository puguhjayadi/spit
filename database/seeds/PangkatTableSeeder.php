<?php

use Illuminate\Database\Seeder;

class PangkatTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table("pangkats")->insert([
    			array(
    				"name" => "Jenderal Besar"), 
    			array(
    				"name" => "Jenderal"), 
    			array(
    				"name" => "Letnan Jenderal"), 
    			array(
    				"name" => "Mayor Jenderal"), 
    			array(
    				"name" => "Brigadir Jenderal"), 
    			array(
    				"name" => "Kolonel"), 
    			array(
    				"name" => "Letnan Kolonel"), 
    			array(
    				"name" => "Mayor"), 
    			array(
    				"name" => "Kapten"), 
    			array(
    				"name" => "Letnan Satu"), 
    			array(
    				"name" => "Letnan Dua"), 
    			array(
    				"name" => "Pembantu Letnan Satu"), 
    			array(
    				"name" => "Pembantu Letnan Dua"), 
    			array(
    				"name" => "Sersan Mayor"), 
    			array(
    				"name" => "Sersan Kepala"), 
    			array(
    				"name" => "Sersan Satu"), 
    			array(
    				"name" => "Sersan Dua"), 
    			array(
    				"name" => "Kopral Kepala"), 
    			array(
    				"name" => "Kopral Satu"), 
    			array(
    				"name" => "Kopral Dua"), 
    			array(
    				"name" => "Prajurit Kepala"), 
    			array(
    				"name" => "Prajurit Satu"), 
    			array(
    				"name" => "Prajurit Dua")
    		]);
    	}
    }
