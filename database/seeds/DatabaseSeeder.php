<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(IndonesiaTableSeeder::class);
        $this->call(KodamTableSeeder::class);
        $this->call(KoremTableSeeder::class);
        $this->call(KodimTableSeeder::class);
        $this->call(KoramilTableSeeder::class);
        $this->call(PangkatTableSeeder::class);
    }
}
