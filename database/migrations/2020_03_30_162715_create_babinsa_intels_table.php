<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBabinsaIntelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('babinsa_intels', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained();
            $table->enum('rank', ['Jenderal Besar']);
            $table->string('nip');
            $table->foreignId('kodam_id')->constrained();
            $table->foreignId('korem_id')->constrained();
            $table->foreignId('kodim_id')->constrained();
            $table->string('koramil_id'); //1,2,3
            $table->foreignId('province_id')->constrained()->nullable();
            $table->foreignId('regency_id')->constrained()->nullable();
            $table->foreignId('district_id')->constrained()->nullable();
            $table->foreignId('village_id')->constrained()->nullable();
            $table->string('address')->nullable();
            $table->string('pos_code')->nullable();
            $table->string('photo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('babinsa_intels');
    }
}
